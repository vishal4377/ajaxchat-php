<?php
require_once __DIR__ . '/core.php';
require_once __DIR__ . '/chat_db.php';
if ($auth->isLoggedIn()) {
    $data['user']=getUserInfo($auth);
    $data['page_title']="Chat Page";
    $data['users']=getVerifiedUsers();
    load_view('includes/inner_header',$data);    
    load_view('chat',$data);    
    load_view('includes/inner_footer',$data);
}
else {
    redirect('index.php');
}
?>