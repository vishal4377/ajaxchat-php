<?php
require_once __DIR__ . '/core.php';

if ($auth->isLoggedIn()) {
    redirect('index.php');
}
else {
    $auth_error=false;
    if(isset($_POST['username']) && isset($_POST['email']) && isset($_POST['password'])){
        try {
            $userId = $auth->registerWithUniqueUsername($_POST['email'], $_POST['password'], $_POST['username']);
            $auth->login($_POST['email'], $_POST['password']);
            redirect('index.php');
            echo 'We have signed up a new user with the ID ' . $userId;
        }
        catch (\Delight\Auth\InvalidEmailException $e) {
            $auth_error='Wrong email address';
        }
        catch (\Delight\Auth\InvalidPasswordException $e) {
            $auth_error='Wrong password';
        }
        catch (\Delight\Auth\UserAlreadyExistsException $e) {
            $auth_error='User already exists';
        }
        catch (\Delight\Auth\TooManyRequestsException $e) {
            $auth_error='Too many requests';
        }
        catch (\Delight\Auth\DuplicateUsernameException $e) {
            $auth_error='Username already exist';
        }
    }
    $data['auth_error']=$auth_error;
    $data['page_title']='Signup';
    load_view('includes/outer_header',$data);
    load_view('auth/register_form',$data);
    load_view('includes/outer_footer');
}
?>