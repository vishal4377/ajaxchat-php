<?php
require_once __DIR__ . '/core.php';

load_view('includes/outer_header');
if ($auth->isLoggedIn()) {
    echo 'Hello '.$auth->getUsername();
    redirect('chat.php');
}
else {
    redirect('login.php');
}
load_view('includes/outer_footer');
?>