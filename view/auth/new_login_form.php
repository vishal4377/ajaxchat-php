<p class="login-box-msg">Login</p>
<form action="" method="post" id="login_form" data-parsley-validate>
          <div class="form-group has-feedback">
          <input type="text" name="login" value="" id="login" class="form-control" placeholder="Email or Username" maxlength="80" size="30" data-parsley-required="true">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
          <input type="password" name="password" value="" id="password" class="form-control" placeholder="Password" size="30" data-parsley-required="true">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          
          <div class="row">
            <div class="col-xs-8">
              <div class="checkbox icheck">
                <label>
                <input type="checkbox" name="remember" value="1" id="remember" style="margin:0;padding:0"  />
                Remember Me
                </label>
              </div>
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
            </div><!-- /.col -->
          </div>
          <div class="row checkbox">
          <div class="col-xs-12">
          <a href="<?php echo site_url('signup.php')?>">Register Now</a>
          </div>
          </div>
          </form>
<?php
if(isset($auth_error) && $auth_error){
  echo '<div class="alert alert-danger" role="alert">'.$auth_error.'</div>';
}
?>