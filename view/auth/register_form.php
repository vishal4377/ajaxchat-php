<p class="login-box-msg">Register Now</p>
<form action="" method="post" id="signup_form" data-parsley-validate>
          <div class="form-group has-feedback">
          <input type="text" name="username" value="" id="username" class="form-control" placeholder="Username" maxlength="80" size="30" data-parsley-required="true">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
          <input type="email" name="email" value="" id="email" class="form-control" placeholder="Email" maxlength="80" size="30" data-parsley-required="true">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
          <input type="password" name="password" value="" id="password" class="form-control" placeholder="Password" size="30" data-parsley-required="true">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          
          <div class="row">
            <div class="col-xs-8">
            <a href="<?php echo site_url('login.php')?>">Login</a>
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Sign Up</button>
            </div><!-- /.col -->
          </div>
          </form>
<?php
if(isset($auth_error) && $auth_error){
  echo '<div class="alert alert-danger" role="alert">'.$auth_error.'</div>';
}
?>