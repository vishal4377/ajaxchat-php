 <footer class="main-footer">
        <div class="pull-right hidden-xs">
         <strong><?php echo config('sitename');?> </strong>
        </div>
        <strong>&copy; <?php echo date('Y');?></strong>
     
     
      </footer>
      </div>
      </body>
       
    <!-- jQuery UI 1.11.4 -->
    
   
    <!-- Bootstrap 3.3.2 JS -->
    <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>" type="text/javascript"></script>
    <script src="<?php echo base_url('assets/js/app.js');?>" type="text/javascript"></script>
<script>
  $(document).ready(function(){
        // to display number of comment pending
    $('.toggle-btn').click(function(){
        if($(this).hasClass('selected')) return false;
        if($(this).hasClass('dis')) return false;
        rival = $(this).attr('data-target'); 
        toBeUpdated = $(this).attr('data-update');
        
        toBeUpdatedValue = $(this).attr('data-value'); 
        
        $(toBeUpdated).val(toBeUpdatedValue).change();
        
        if($(this).hasClass('selected')){
            $(this).removeClass('selected').addClass('unselected');
            $(rival).removeClass('unselected').addClass('selected');
        }
        else
        {
           $(this).removeClass('unselected').addClass('selected');
           $(rival).removeClass('selected').addClass('unselected');  
        }
       }); 
       
       
    $(".decimal").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
             // Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
             // Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    
    $(".integer").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46,8, 9, 27, 13]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
             // Allow: Ctrl+C
            (e.keyCode == 67 && e.ctrlKey === true) ||
             // Allow: Ctrl+X
            (e.keyCode == 88 && e.ctrlKey === true) ||
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105 || e.keyCode == 110 || e.keyCode == 190 ) ) {
            e.preventDefault();
        }
    });
       
      
    });//ready
    


        
           
 </script>   
