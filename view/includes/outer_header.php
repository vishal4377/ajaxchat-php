<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
 <!DOCTYPE html>
<html>
  <head>
  <title><?php echo (isset($page_title)?$page_title.' - ':'').config('sitename'); ?></title>
    <meta charset="UTF-8">
    
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Theme style -->
    <link href="<?php echo base_url(); ?>assets/css/AdminLTE.css" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="<?php echo base_url(); ?>assets/plugins/iCheck/square/blue.css" rel="stylesheet" type="text/css" />
    
    <!--Parsley.css-->
        <link href="<?php echo base_url('assets/css/parsley.css') ?>" rel="stylesheet" type="text/css" />
        <script src="<?php echo base_url('assets/js/jquery-1.10.2.min.js')?>"></script>
        <script src="<?php echo base_url('assets/js/parsley.min.js')?>"></script>
    
    <script src="<?php echo base_url('assets/plugins/pace/pace.js') ?>"></script>
    <link href="<?php echo base_url('assets/plugins/pace/themes/blue/pace-theme-mac-osx.css') ?>" rel="stylesheet" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
      .login-page{
        background: url('https://images.pexels.com/photos/1111368/pexels-photo-1111368.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940');background-size: 100% 100%;
      }
    </style>
  </head>
  <body class="login-page">
    <div class="login-box" style="box-shadow: 0px 2px 10px #000">
     <div class="login-box-body">
    <div class="text-center" style="margin-bottom: 20px;">
      <a href="<?php echo site_url(); ?>"><!--<img src="<?php echo base_url('assets/images/logo.png'); ?>" style="width: 320px;margin: auto;"/>-->
      <h2><?php echo config('sitename'); ?></h2>
      </a>       
    
    </div>
