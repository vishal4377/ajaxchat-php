<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title><?php echo (isset($page_title)?$page_title.' - ':'').config('sitename'); ?></title>

    <script src="<?php echo base_url('assets/plugins/pace/pace.js') ?>"></script>
    <link href="<?php echo base_url('assets/plugins/pace/themes/white/pace-theme-flash.css') ?>" rel="stylesheet" />
    <!-- Tell the browser to be responsive to screen width -->
        <!-- bootstrap.css -->
        <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
               
         <!--Jquery-ui.css-->
        <link href="<?php echo base_url('assets/css/jquery-ui.min.css') ?>" rel="stylesheet" type="text/css" />
        
        <!--Parsley.css-->
        <link href="<?php echo base_url('assets/css/parsley.css') ?>" rel="stylesheet" type="text/css" />
        <!-- Admin LTE Theme style -->
        <link href="<?php echo base_url('assets/css/AdminLTE.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/css/_all-skins.css') ?>" rel="stylesheet" type="text/css" />
        <!-- Custom style inner.css -->
        <link href="<?php echo base_url('assets/css/inner.css') ?>" rel="stylesheet" type="text/css" />
        <!-- jquery 1.10.2 -->
        <script src="<?php echo base_url('/assets/js/jquery-1.10.2.min.js')?>"></script>
        <script src="<?php echo base_url('assets/js/jquery-migrate-1.2.1.min.js')?>"></script>
        <script src="<?php echo base_url('assets/js/jquery-ui.min.js')?>"></script>
        <script src="<?php echo base_url('assets/js/parsley.min.js')?>"></script>
        
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  
   <body class="skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="<?php echo site_url('');?>" class="logo" title="<?php echo config('sitename');?>">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><?php echo substr(config('sitename'),0,1);?>
           <!--<img src="<?php echo base_url('assets/images/logo-small.png'); ?>" style="width:40px;margin: auto;"/>     
            -->
          </span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><?php echo config('sitename');?>
          <!--<img src="<?php echo base_url('assets/images/logo.png'); ?>" style="width: 215px;margin: auto;margin-left: -7px;"/> -->    
          </span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
             
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-user"></i>
                 <?php echo htmlentities($user['username']) ?>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header" >
                   <p>
                   <center><img src="https://api.adorable.io/avatars/50/<?php echo htmlentities($user['username']) ?>" alt="" class="img img-responsive img-circle"></center>
                      <?php echo htmlentities($user['username']) ?>
                    </p>
                  </li>
                 <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <!-- <a href="<?php echo site_url('settings');?>" class="btn btn-default btn-flat">Settings</a> -->
                    </div>
                    <div class="pull-right">
                      <a href="<?php echo site_url('logout.php');?>" class="btn btn-default btn-flat">Logout</a>
                    </div>
                  </li>
                </ul>
             
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
       
        <section class="sidebar">
         <ul class="sidebar-menu">
            <li class="active">
              <a href="<?php echo site_url('chat.php');?>">
                <i class="fa fa-comment"></i> <span>Chat</span> </a>
            </li>
            <li>
                <a href="<?php echo site_url('logout.php')?>">
                    <i class="fa fa-power-off "></i> <span>Logout</span>
                </a>
            </li>
            

        </ul>
        </section>
        <!-- /.sidebar -->
      </aside>
      
       <script>
        $(document).ready(function(){
       
        });
      
      </script>
      