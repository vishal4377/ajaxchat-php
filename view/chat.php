 <style>
 .direct-chat-contacts-x{
  background: #222d32;
    color: #fff;
    overflow: auto;
    height:70vh;
 }
 .direct-chat-messages{
  height:56vh;
 }
 </style>
<div class="content-wrapper" style="min-height: 916px;background: white;">
        <!-- Content Header (Page header) 
        <section class="content-header">
          <h1>
            Dashboard
            <small></small>
          </h1>
          <ol class="breadcrumb">
           
          </ol>
        </section>-->
                <!-- Main content -->
        <section class="content">
        <div class="row">
            <div class="col-md-4">
            <div class="direct-chat-contacts-x">
                    <ul class="contacts-list">
                    <?php
                    foreach ($users as $contact) {
                      ?>
                      <li data-uid="<?php echo ucfirst($contact['id'])?>" class="chat_contact">
                        <a href="#">
                          <img class="contacts-list-img" src="https://api.adorable.io/avatars/128/<?php echo($contact['email'])?>" alt="User Image">
                          <div class="contacts-list-info">
                                <span class="contacts-list-name">
                                  <?php echo ucfirst($contact['username'])?>
                                </span>
                          </div>
                          <!-- /.contacts-list-info -->
                        </a>
                      </li>
                      <!-- End Contact Item -->
                      
                      <?php
                    }
                    ?>
                      
                    </ul>
                    <!-- /.contatcts-list -->
                  </div>
                  <!-- /.direct-chat-pane -->
            </div>
            <div class="col-md-8">
              <!-- DIRECT CHAT -->
              <div class="box box-warning direct-chat direct-chat-warning">
                <div class="box-header with-border">
                  <h3 class="box-title" id="chat_title">Chat</h3>

                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <!-- Conversations are loaded here -->
                  <div class="direct-chat-messages" id="chat_message">
                    Click on user from side list
                  </div>
                  <!--/.direct-chat-messages-->

                  <!-- Contacts are loaded here -->
                  
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <form action="#" method="post" id="send_chat">
                    <div class="input-group">
                    <input type="hidden" name="to" value="0" id="chat_other_user">
                      <input type="text" name="msg" placeholder="Type Message ..." class="form-control" id="message_input">
                      <span class="input-group-btn">
                            <button type="submit" class="btn btn-warning btn-flat">Send</button>
                          </span>
                    </div>
                  </form>
                </div>
                <!-- /.box-footer-->
              </div>
              <!--/.direct-chat -->
            </div>
            <!-- /.col -->
            </div>
            <!-- .row -->
        </section><!-- /.content -->
        
      </div>
<script>
  var chat_to=false;
  $( document ).ready(function() {
  
  $('#send_chat').submit(function (e){
    e.preventDefault();
    // console.log($(this).serialize())
    var _form=$(this);
    if(chat_to){
    PostChatData(_form);
    }else{
      alert('Select a contact first.');
    }
    setTimeout(() => {
      $('#chat_message').scrollTop($('#chat_message')[0].scrollHeight);
    }, 1000);
  });

  $('.chat_contact').click(function (e){
    var to = $(this).data('uid');
    $('.direct-chat-messages').html('Loading...');
    GetChatData(to);
    chat_to=to;
    $('#chat_other_user').val(to);
    setTimeout(() => {
      $('#chat_message').scrollTop($('#chat_message')[0].scrollHeight);
    }, 1000);
  });
});
setInterval(function(){ 
  if(chat_to){
    GetChatData(chat_to);
  }
}, 300);
function GetChatData(to){
  var chatrequest = $.ajax({
      url: "<?php echo site_url('ajax.php?req=getchat')?>",
      method: "POST",
      data: { 'to' : to },
      dataType: "json"
    });
    
    chatrequest.done(function( msg ) {
      var chat=``;
      $.each(msg,function(i,v){
        if(v.from_id==<?php echo $user['id'];?>){
          chat+=`<div class="direct-chat-msg right">
                      <div class="direct-chat-info clearfix">
                        <span class="direct-chat-name pull-right">${v.from_username}</span>
                        <span class="direct-chat-timestamp pull-left">${v.created}</span>
                      </div>
                      <img class="direct-chat-img" src="https://api.adorable.io/avatars/128/${v.from_email}" alt="${v.from_username}">
                      <div class="direct-chat-text">
                      ${v.content}
                      </div>
                    </div>
                    </div>
                  </div>
                  `;
        }else{
        chat+=`<div class="direct-chat-msg">
                    <div class="direct-chat-info clearfix">
                      <span class="direct-chat-name pull-left">${v.from_username}</span>
                      <span class="direct-chat-timestamp pull-right">${v.created}</span>
                    </div>
                    <img class="direct-chat-img" src="https://api.adorable.io/avatars/128/${v.from_email}" alt="${v.from_username}">
                    <div class="direct-chat-text">
                    ${v.content}
                    </div>
                  </div>
                `;
        }
      })
      $( ".direct-chat-messages" ).html( chat );
    });
    
    chatrequest.fail(function( jqXHR, textStatus ) {
      alert( "Request failed: " + textStatus );
    });
}

function PostChatData(_form){
  var chatrequest = $.ajax({
      url: "<?php echo site_url('ajax.php?req=postchat')?>",
      method: "POST",
      data: _form.serialize(),
      dataType: "json"
    });
    
    chatrequest.done(function( msg ) {
      console.log(msg);
      $('#message_input').val('');
    });
    
    chatrequest.fail(function( jqXHR, textStatus ) {
      alert( "Request failed: " + textStatus );
    });
}
</script>