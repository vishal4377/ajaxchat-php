<?php
require_once __DIR__ . '/core.php';

// $db

function getVerifiedUsers(){
    global $db;
    $stmt = $db->prepare("SELECT `id`,`email`,`username`,`verified` FROM `users` WHERE `verified` = 1");
    $stmt->execute();
    $stmt->setFetchMode(PDO::FETCH_ASSOC);
    return $stmt->fetchAll();
}

function getChat($to,$from){
    markReadAll($to,$from);
    global $db;
    // $stmt = $db->prepare("SELECT * FROM `personal_message` WHERE (`to_id` = $to AND `from_id` = $from) OR (`to_id` = $from AND `from_id` = $to)");
    $stmt = $db->prepare("SELECT `personal_message`.*, fromuser.username as from_username,fromuser.email as from_email FROM `personal_message`
    LEFT JOIN users AS fromuser ON `personal_message`.`from_id` = `fromuser`.`id`
    WHERE (`to_id` = $to AND `from_id` = $from) OR (`to_id` = $from AND `from_id` = $to)");
    // $stmt = $db->prepare("SELECT `personal_message`.*,touser.username as to_username,touser.email to_email,fromuser.username as from_username,fromuser.email as from_email FROM `personal_message`
    // LEFT JOIN users AS touser ON `personal_message`.`to_id` = `touser`.`id`
    // LEFT JOIN users AS fromuser ON `personal_message`.`from_id` = `fromuser`.`id`
    // WHERE (`to_id` = $to AND `from_id` = $from) OR (`to_id` = $from AND `from_id` = $to)");
    $stmt->execute();
    $stmt->setFetchMode(PDO::FETCH_ASSOC);
    return $stmt->fetchAll();
}

function markReadAll($to,$from){
    global $db;
    $stmt = $db->prepare("UPDATE `personal_message` SET `is_read`=1 WHERE `to_id` = $from AND `from_id` = $to");
    $stmt->execute();
    $stmt->setFetchMode(PDO::FETCH_ASSOC);
    return $stmt->fetchAll();
}

function insert_messsage($to,$from,$msg){
    global $db;
    $stmt = $db->prepare("INSERT INTO `personal_message` ( `to_id`, `from_id`, `content`) VALUES (".$to.",".$from.",'".$msg."');");
    $stmt->execute();
    $stmt->setFetchMode(PDO::FETCH_ASSOC);
    return $stmt->fetchAll();
}

?>