<?php
require_once __DIR__ . '/core.php';
if ($auth->isLoggedIn()) {
    redirect('index.php');
}
else {
    $auth_error=false;
    if(isset($_POST['login']) && isset($_POST['password'])){
        try {
            $auth->login($_POST['login'], $_POST['password']);
            redirect('index.php');
        }
        catch (\Delight\Auth\InvalidEmailException $e) {
            $auth_error='Wrong email address';
        }
        catch (\Delight\Auth\InvalidPasswordException $e) {
            $auth_error='Wrong password';
        }
        catch (\Delight\Auth\EmailNotVerifiedException $e) {
            $auth_error='Email not verified';
        }
        catch (\Delight\Auth\TooManyRequestsException $e) {
            $auth_error='Too many requests';
        }
    }
    $data['auth_error']=$auth_error;
    $data['page_title']='Login';
    load_view('includes/outer_header',$data);
    load_view('auth/new_login_form',$data);
    load_view('includes/outer_footer');
}
?>