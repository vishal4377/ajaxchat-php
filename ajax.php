<?php
require_once __DIR__ . '/core.php';
require_once __DIR__ . '/chat_db.php';
if ($auth->isLoggedIn()) {
    $action = @$_GET['req'];
    $userdata=getUserInfo($auth);
    switch ($action) {
        case 'getchat':
            $to=$_REQUEST['to'];
            $from=$userdata['id'];
            $chat=getChat($to,$from);
            header('Content-Type: application/json');
            echo json_encode($chat);
            break;
        
        case 'postchat':
            if(isset($_POST['to']) && isset($_POST['msg'])){
                $to=$_REQUEST['to'];
                $from=$userdata['id'];
                $msg=$_REQUEST['msg'];
                insert_messsage($to,$from,$msg);
            }
            // insert_messsage($to,$from,$msg)
            break;
        
        default:
            # code...
            break;
    }
}
else {
    redirect('index.php');
}
?>