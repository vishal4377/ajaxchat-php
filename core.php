<?php
require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/config.php';

define('BASEPATH',__DIR__);
define('BASEURL','http://localhost/vs/misc/chatapp/');

$db = new \PDO('mysql:dbname='._CONFIG['db']['dbname'].';host='._CONFIG['db']['host'].';charset='._CONFIG['db']['charset'].'', _CONFIG['db']['username'], _CONFIG['db']['password']);
$auth = new \Delight\Auth\Auth($db);

function base_url($path=''){
    return BASEURL.$path;
}

function site_url($path=''){
    return BASEURL.$path;
}

function load_view($path, $variables = array(), $print = true){
    includeWithVariables(__DIR__.'/view/'.$path.'.php',$variables,$print);
}
function includeWithVariables($filePath, $variables = array(), $print = true)
{
    $output = NULL;
    if(file_exists($filePath)){
        // Extract the variables to a local namespace
        extract($variables);

        // Start output buffering
        ob_start();

        // Include the template file
        include $filePath;

        // End buffering and return its contents
        $output = ob_get_clean();
    }
    if ($print) {
        print $output;
    }
    return $output;

}
function config(){
    $args=func_get_args();
    $val=false;
    $configarr=_CONFIG;
    for($i=0;$i<func_num_args();$i++){
        if(isset($configarr[$args[$i]])){
            $configarr=@$configarr[$args[$i]];
        }else{
            $configarr=false;
        }
    }
    $val= $configarr;
    return $val;
}

function redirect($address=''){
    header('Status: 301 Moved Permanently', false, 301);
    header('Location: '.$address);
    exit();
}

function getUserInfo($auth){
    return array(
        'username'=>$auth->getUsername(),
        'id'=>$auth->getUserId(),
        'email'=>$auth->getEmail(),
        'ip' => $auth->getIpAddress(),
    );
}
?>